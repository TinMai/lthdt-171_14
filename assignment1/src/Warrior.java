public class Warrior extends Fighter {

	public Warrior(int baseHp, int wp) {
		super(baseHp, wp);
	}

	@Override
	public double getCombatScore() {
		if (Utility.isPrime(Battle.GROUND)==true) {
			return getBaseHp()*2;
		}
		else {
			if (getWp()==1) {
				return getBaseHp();
			}
			else
				return getBaseHp()/10;
	}
	}
}
