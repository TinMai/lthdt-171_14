public class DeathEater extends Monster implements Combatable{

	public DeathEater(Complex mana) {
		super(mana);
	}

	@Override
	public double getCombatScore() {
		return Math.sqrt(Complex.getX()*Complex.getX()+Complex.getY()*Complex.getY());
	}

	

}
